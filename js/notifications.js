/*
 * Creates a notice box (like an Android toast message). Notices should only be used to show information to the 
 * user where user feedback-interaction is not needed (e.g. "invalid passowrd" or "save successful" 
 * or "you have a new message"). 
 * 
 * Notices can have an icon, a message and/or a contextual status accosiated with them.
 * statuses include:
 * 		success
 * 		error
 * 		warn
 *		info
 * Any other status name will default to nothing and the default theme persists. 
 *
 * Icons are passed in as class name using icons from FontAwesome (e.g. 'fa-comments'). 
 * Message is a string of textual information for the user, it should be short and to the point.
 *
 * For notifications that require or allow for user feedback (e.g. "successfully deleted life | undo")
 * please use Alert (works like Android Snackbar).
 * 
 */

var notice = {
	load: function () {
		$("body").on("click", ".notification__toggle", notice.show );

		$("body").append("<div class='notice' />");
		$(".notice").append("<div class='notice__icon' />");
			$(".notice__icon").append("<span class='fa' />");
		$(".notice").append("<div class='notice__message' />");
		$(".notice").hide();
	},
	show: function (icon, message, status) {
		icon = typeof icon !=='undefined' ? icon : 'fa-info-circle';
		message = typeof message !=='undefined' ? message : 'Default message: No message provided.';
		status = typeof status !=='undefined' ? status : 'default';

		switch (status) {
			case 'success' :
				$(".notice").addClass("success");
				break;
			case 'error' :
				$(".notice").addClass("error");
				break;
			case 'warn' :
				$(".notice").addClass("warn");
				break;
			case 'info' :
				$(".notice").addClass("info");
				break;
			case 'default':
			default:
				break;
		}

		$(".notice__icon .fa").addClass(icon);
		$(".notice__message").append("<pre>" + message + "</pre>");
 		
 		$(".notice").fadeIn();
		setTimeout(
			function() {
				notice.hide(icon, message, status);
			}, 3000);
		return;
	},
	hide: function(icon, message, status) {
		$(".notice").fadeOut(function() {
			$(".notice").removeClass(status);
			$(".notice__icon .fa").removeClass(icon);
			$(".notice__message pre").remove();
		});
		
	}
}

var alert = {
	load: function () {
		$("body").on("click", ".alert__toggle", alert.show );

		$("body").append("<div class='alert' />");
		$(".alert").append("<div class='alert__icon' />");
			$(".alert__icon").append("<span class='fa' />");
		$(".alert").append("<div class='alert__message' />");
		$(".alert").append("<div class='alert__action' />");
		$(".alert__action").text("UNDO");
		$(".alert").hide();
	},
	show: function (icon, message, status) {
		icon = typeof icon !=='undefined' ? icon : 'fa-info-circle';
		message = typeof message !=='undefined' ? message : 'Default message: No message provided.';
		status = typeof status !=='undefined' ? status : 'default';

		switch (status) {
			case 'success' :
				$(".alert").addClass("success");
				break;
			case 'error' :
				$(".alert").addClass("error");
				break;
			case 'warn' :
				$(".alert").addClass("warn");
				break;
			case 'info' :
				$(".alert").addClass("info");
				break;
			case 'default':
			default:
				break;
		}

		$(".alert__icon .fa").addClass(icon);
		$(".alert__message").append("<pre>" + message + " </pre>"); 
		$(".alert").fadeIn();

		setTimeout(
			function() {
				alert.hide(icon, message, status);
			}, 6000);
		return;
	},
	hide: function(icon, message, status) {
		$(".alert").fadeOut(function(){
			$(".alert").removeClass(status);
			$(".alert__icon .fa").removeClass(icon);
			$(".alert__message pre").remove();
		});
	}
}

var modal = {
	load: function () {
		$("body").on("click", ".modal__toggle", modal.show );
		$("body").on("click", ".modal__header .modal__icon", modal.hide );
		$("body").on("click", ".modal__footer button[data-type='neutral']", modal.hide );
		$("body").append("<div class='modal' />");
		$(".modal").append("<div class='modal__container' />");
			$(".modal__container").append("<div class='modal__header' />");
				$(".modal__header").append("<h1 class='modal__title' />");
				$(".modal__header").append("<div class='modal__icon' />");
					$(".modal__icon").append("<span class='fa fa-times' />");;
			$(".modal__container").append("<div class='modal__body' />");
			$(".modal__container").append("<div class='modal__footer' />");
		$(".modal").hide();
	},
	show: function(title) {
		title = typeof title !=='undefined' ? title : 'Modal Title';


		$(".modal__title").text(title);
		$(".modal__footer").append("<button />");
		$(".modal__footer button").attr("data-type", "neutral");
		$(".modal__footer button").text("close");

		$(".modal").fadeIn();
	},
	hide: function() {
		$(".modal").fadeOut(function(){
			$(".modal__title").text("");
			$(".modal__footer button").remove();
		});
	}
}


$(document).ready(function(){
	if ($(".tooltip").length) {
		$(".tooltip").each(function(index){
			$(this).attr('tabindex', index + 1);
		});
	}
});