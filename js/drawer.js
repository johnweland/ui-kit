var drawer = {
    load: function() {
        $('body').on('click', '.drawer__toggle', drawer.toggle);
        $('.drawer__panel').hide();
    },
    toggle: function (effect, direction, duration) {
        $('.drawer__panel').toggle();
    }
}